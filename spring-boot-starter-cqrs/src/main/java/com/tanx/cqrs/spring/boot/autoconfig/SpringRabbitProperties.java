package com.tanx.cqrs.spring.boot.autoconfig;

import com.tanx.cqrs.infrastructure.spring.event.RabbitMqSetting;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ConfigurationProperties(prefix = "spring.cqrs.rabbitmq")
public class SpringRabbitProperties extends RabbitMqSetting {
}
