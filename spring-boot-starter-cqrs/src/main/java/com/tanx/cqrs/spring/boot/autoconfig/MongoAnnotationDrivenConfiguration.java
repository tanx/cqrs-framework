package com.tanx.cqrs.spring.boot.autoconfig;

import com.tanx.cqrs.saga.SagaStoreRepository;
import com.tanx.cqrs.saga.impl.MongoSagaStoreRepositoryImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置mongodb
 */
//@Deprecated
@Configuration
@EnableConfigurationProperties(SpringMongoProperties.class)
public class MongoAnnotationDrivenConfiguration {

//    @ConditionalOnClass(MongoSagaStoreRepositoryImpl.class)
//    @ConditionalOnMissingBean
//    @Bean
//    public SagaStoreRepository repository(SpringMongoProperties mongoProperties) {
//        return new MongoSagaStoreRepositoryImpl(mongoProperties.getSagaCollectionName());
//    }
}