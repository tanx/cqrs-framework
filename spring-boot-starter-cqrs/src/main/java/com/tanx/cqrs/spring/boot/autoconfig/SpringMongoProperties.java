package com.tanx.cqrs.spring.boot.autoconfig;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 */
@EqualsAndHashCode()
@Data
@ConfigurationProperties(prefix = "spring.cqrs.mongo")
public class SpringMongoProperties {
    private String sagaCollectionName;
}
