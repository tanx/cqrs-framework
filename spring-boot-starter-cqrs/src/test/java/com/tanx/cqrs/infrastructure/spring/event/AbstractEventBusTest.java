package com.tanx.cqrs.infrastructure.spring.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

public class AbstractEventBusTest {

    @Test
    public void parseEventFormByte() throws Exception {
        TestEvent testEvent = new TestEvent();
        EventProxy eventProxy = new EventProxy(testEvent, this.getClass());
        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writeValueAsString(eventProxy);

        EventProxy eventProxy1 = objectMapper.readValue(s, EventProxy.class);
        System.out.println(eventProxy1);
    }

}