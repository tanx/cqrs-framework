package com.tanx.cqrs.infrastructure.spring.event;

import com.tanx.cqrs.event.Event;

import java.util.UUID;

/**
 * Created by tangxu on 18-1-19.
 */
public class TestEvent implements Event {
    private String uuid = UUID.randomUUID().toString().replaceAll("-", "");
    private String aggregateId = UUID.randomUUID().toString().replaceAll("-", "");

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public String getAggregateId() {
        return aggregateId;
    }

    @Override
    public String getSagaId() {
        return null;
    }

    @Override
    public void setSagaId(String s) {

    }
}
