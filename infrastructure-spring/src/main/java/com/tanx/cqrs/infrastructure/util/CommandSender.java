package com.tanx.cqrs.infrastructure.util;

import com.tanx.cqrs.command.Command;
import com.tanx.cqrs.command.CommandBus;
import com.tanx.cqrs.command.CommandCallback;
import lombok.Data;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 事件发送器
 */
@Data
public class CommandSender {
    public static CommandBus BUS;

    private CommandSender() {
    }


    public static void send(Command command, CommandCallback<? super Command, Object> commandCallback) {
        BUS.send(command, commandCallback);
    }

    public static Object sendAndWait(Command command) throws ExecutionException, InterruptedException {
        return BUS.sendAndWait(command);
    }

    public static Object sendAndWait(Command command, long time, TimeUnit timeUnit) {
        return BUS.sendAndWait(command, time, timeUnit);
    }

    public static CompletableFuture<Object> send(Command command) {
        return BUS.send(command);
    }
}
