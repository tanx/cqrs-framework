package com.tanx.cqrs.infrastructure.util;

import com.tanx.cqrs.event.Event;
import com.tanx.cqrs.event.EventBus;
import com.tanx.cqrs.saga.Saga;
import lombok.Data;

/**
 * 事件发送器
 */
@Data
public class EventSender {
    public static EventBus BUS;

    private EventSender() {
    }


    public static void send(Event event) {
        BUS.sendEvent(event);
    }

    public static void sendSagaEvent(Event event, Saga saga) {
        BUS.sendSagaEvent(event, saga);
    }
}
