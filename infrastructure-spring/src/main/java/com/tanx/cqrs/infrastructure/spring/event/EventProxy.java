package com.tanx.cqrs.infrastructure.spring.event;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tanx.cqrs.event.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 事件包装类,用来代理事件和事件产生的聚合的关系
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonDeserialize(using = EventProxyDeserializer.class)
public class EventProxy {
    private Event event;
    private Class<?> eventClass;
    private Class<?> senderClass;
    private String sagaId;

    public EventProxy(Event event, Class<?> sourceClass) {
        this.event = event;
        this.eventClass = event.getClass();
        this.senderClass = sourceClass;
    }
}
