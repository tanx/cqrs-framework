package com.tanx.cqrs.infrastructure.spring.event;

import com.tanx.cqrs.event.RoutingKeyGenerator;

/**
 * 根据事件包名称生成路由key
 */
public class PackageRoutingKeyGenerator implements RoutingKeyGenerator {
    @Override
    public String generate(Object event) {
        return event.getClass().getName();
    }
}
