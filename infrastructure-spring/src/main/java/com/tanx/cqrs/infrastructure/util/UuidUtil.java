package com.tanx.cqrs.infrastructure.util;

import java.util.UUID;

/**
 * 生成uuid,并去除-
 */
public class UuidUtil {

    public static String getUuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
