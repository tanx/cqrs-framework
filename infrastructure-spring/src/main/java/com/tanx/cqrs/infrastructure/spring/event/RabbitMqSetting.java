package com.tanx.cqrs.infrastructure.spring.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 连接信息
 */
@Data
@Builder
@AllArgsConstructor
public class RabbitMqSetting {
    private String host;
    private int port;
    private String username;
    private String password;
    private String exchangeName;
    private String routingKey;
    private int basicQos;

    public RabbitMqSetting() {
    }
}
