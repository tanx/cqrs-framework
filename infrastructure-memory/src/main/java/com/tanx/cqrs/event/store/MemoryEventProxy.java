package com.tanx.cqrs.event.store;

import com.tanx.cqrs.event.Event;
import lombok.Data;

import java.util.Date;

@Data
public class MemoryEventProxy implements Event {

    private Event event;

    private Date createDateTime = new Date();
    private EventStatus status = EventStatus.UNFINISH;

    public MemoryEventProxy() {
    }

    public MemoryEventProxy(Event eventProxy) {
        this.event = eventProxy;
    }

    @Override
    public String getUuid() {
        return event.getUuid();
    }

    @Override
    public String getAggregateId() {
        return event.getAggregateId();
    }

    @Override
    public String getSagaId() {
        return event.getSagaId();
    }

    @Override
    public void setSagaId(String s) {
        event.setSagaId(s);
    }
}
