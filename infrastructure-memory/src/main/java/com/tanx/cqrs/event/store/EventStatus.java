package com.tanx.cqrs.event.store;

import lombok.Getter;

/**
 * 事件状态
 */
@Getter
public enum EventStatus {
    FINISH("已完成"), UNFINISH("未完成");

    private String label;

    EventStatus(String label) {
        this.label = label;
    }
}
