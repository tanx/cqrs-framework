package com.tanx.cqrs.saga.impl;

import lombok.Getter;

/**
 * 长时处理过程枚举类
 */
@Getter
public enum SagaStatus {
    FINISH("已完成"), START("开始");

    private final String label;

    SagaStatus(String label) {
        this.label = label;
    }


}
