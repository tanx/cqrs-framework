package com.tanx.cqrs.eventsourcing.snapshot;

import java.util.List;


public class MemorySnapshotBuildStrategy implements SnapshotBuildStrategy {
    @Override
    public boolean needCreate(Object o, List list) {
        return false;
    }

    @Override
    public void createSnap(Object o, Object o2) {

    }

    @Override
    public SnapshotRepository getSnapshotRepository() {
        return null;
    }
}
