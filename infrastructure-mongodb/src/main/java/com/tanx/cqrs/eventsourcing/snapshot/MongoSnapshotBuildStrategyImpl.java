package com.tanx.cqrs.eventsourcing.snapshot;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * mongo快照生成策略
 */
@Data
public class MongoSnapshotBuildStrategyImpl<T> implements SnapshotBuildStrategy<T> {
    @Autowired
    private SnapshotRepository<T> repository;

    private int maxEventCount;

    @Override
    public boolean needCreate(T aggregate, List eventList) {
        return eventList != null && eventList.size() > maxEventCount;
    }

    @Override
    public void createSnap(T aggregate, Object id) {
        Snapshot snapshot = new MongoSnapshotImpl(aggregate, id);
        repository.save(snapshot);
    }

    @Override
    public SnapshotRepository<T> getSnapshotRepository() {
        return repository;
    }
}
