package com.tanx.cqrs.event.store;

import com.tanx.cqrs.event.Event;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Mongodb 中的event对象
 */
@Data
@Document
public class MongoEvent {
    @Id
    private String uuid;
    private Event event;
    private Date createDateTime;
    private String aggregateId;
    private String sagaId;
    private EventStatus status = EventStatus.UNFINISH;

    public MongoEvent(Event proxy) {
        this.event = proxy;
        this.uuid = this.event.getUuid();
        this.createDateTime = new Date();
        this.aggregateId = this.event.getAggregateId();
        this.sagaId = proxy.getSagaId();
    }

    public MongoEvent() {
    }
}
