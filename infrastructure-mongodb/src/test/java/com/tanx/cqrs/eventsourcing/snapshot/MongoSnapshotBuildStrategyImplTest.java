package com.tanx.cqrs.eventsourcing.snapshot;

import com.tanx.cqrs.event.store.AggregateTest;
import com.tanx.cqrs.event.store.BaseTestContext;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class MongoSnapshotBuildStrategyImplTest extends BaseTestContext {
    private MongoSnapshotBuildStrategyImpl<AggregateTest> repository;
    private MongoSnapshotRepositoryImpl<AggregateTest> snapshotRepository;

    @BeforeClass
    public void initRepository() {
        repository = new MongoSnapshotBuildStrategyImpl<>();
        repository.setMaxEventCount(2);
        snapshotRepository = new MongoSnapshotRepositoryImplTest().getSnapshotRepository(this.mongoTemplate);
        repository.setRepository(snapshotRepository);
    }

    @Test
    public void testNeedCreate() throws Exception {
        ArrayList<Object> eventList = new ArrayList<>(3);
        eventList.add(new Object());
        eventList.add(new Object());
        eventList.add(new Object());
        boolean b = repository.needCreate(new AggregateTest(), eventList);
        Assert.isTrue(b, "返回布尔值错误");
        eventList.remove(1);
        b = repository.needCreate(new AggregateTest(), eventList);
        Assert.isTrue(!b, "返回布尔值错误");
    }

    @Test
    public void testCreateSnap() throws Exception {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        repository.createSnap(new AggregateTest(), uuid);
        List<MongoSnapshotImpl> list = mongoTemplate.findAll(MongoSnapshotImpl.class, snapshotRepository.getCollectionName());
        Assert.notNull(list, "没有查找到数据");
        Assert.isTrue(!list.isEmpty(), "集合为空");
    }

}