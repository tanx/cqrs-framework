package com.tanx.cqrs.eventsourcing.snapshot;

import com.tanx.cqrs.event.store.AggregateTest;
import com.tanx.cqrs.event.store.BaseTestContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

/**
 *
 */
public class MongoSnapshotRepositoryImplTest extends BaseTestContext {
    private MongoSnapshotRepositoryImpl<AggregateTest> snapshotRepository;

    @BeforeClass
    public void initRepository() {
        snapshotRepository = getSnapshotRepository(this.mongoTemplate);
    }

    @Test
    public void testFindTop1SnapByCreateDateTimeDesc() throws Exception {
        testSave();
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        MongoSnapshotImpl snapshot = new MongoSnapshotImpl(new AggregateTest(), uuid);
        snapshotRepository.save(snapshot);
        Snapshot snap = snapshotRepository.findTop1SnapByCreateDateTimeDesc(uuid);
        Assert.notNull(snap, "未找到快照");
        Assert.notNull(snap.toAggregate(), "转换为聚合失败");
    }

    @Test
    public void testSave() throws Exception {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        MongoSnapshotImpl snapshot = new MongoSnapshotImpl(new AggregateTest(), uuid);
        snapshotRepository.save(snapshot);
        List<MongoSnapshotImpl> list = mongoTemplate.findAll(MongoSnapshotImpl.class, snapshotRepository.getCollectionName());
        Assert.notNull(list, "没有查找到数据");
        Assert.isTrue(!list.isEmpty(), "集合为空");
    }

    @Test
    public void testGetAggregate() throws Exception {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        MongoSnapshotImpl snapshot = new MongoSnapshotImpl(new AggregateTest(), uuid);
        Object o = snapshot.toAggregate();
        Assert.notNull(o, "转化成聚合失败");
    }

    public MongoSnapshotRepositoryImpl<AggregateTest> getSnapshotRepository(MongoTemplate mongoTemplate) {
        MongoSnapshotRepositoryImpl<AggregateTest> repository = new MongoSnapshotRepositoryImpl<>();
        repository.setCollectionName("snapshot");
        repository.setMongoTemplate(mongoTemplate);
        return repository;
    }
}