package com.tanx.cqrs.event.store;

import com.mongodb.DB;
import com.mongodb.Mongo;
import org.testng.annotations.Test;

import java.util.Set;

/**
 *
 */
public class MongoConnectionTest {
    @Test
    public void testConnection() {
        Mongo mongo = new Mongo("52.80.179.11", 32768);
        DB db = mongo.getDB("local");
        Set<String> collectionNames = db.getCollectionNames();
        for (String name : collectionNames) {
            System.out.println("collectionName:" + name);
        }
    }
}
