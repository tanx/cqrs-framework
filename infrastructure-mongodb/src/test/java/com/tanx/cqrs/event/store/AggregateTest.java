package com.tanx.cqrs.event.store;

import lombok.Data;

import java.io.Serializable;

/**
 *
 */
@Data
public class AggregateTest implements Serializable {

    private String uuid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AggregateTest test = (AggregateTest) o;

        return uuid != null ? uuid.equals(test.uuid) : test.uuid == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }
}
