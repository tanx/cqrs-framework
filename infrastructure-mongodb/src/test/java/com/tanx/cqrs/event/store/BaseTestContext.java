package com.tanx.cqrs.event.store;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

/**
 * 初始化mongoTemplate
 */
public class BaseTestContext {
    protected MongoTemplate mongoTemplate;
    private MongoClient mongoClient;
    private String dbName = "mongo-test";

    @BeforeClass
    public void initMongoTemplate() throws IOException {
        String username = "";
        String password = "";
        MongoCredential credential = MongoCredential.createCredential(username, dbName, password.toCharArray());
//        MongoClient mongoClient = new MongoClient(new ServerAddress("host1", 27017), Collections.singletonList(credential));
        mongoClient = new MongoClient(new ServerAddress("52.80.179.11", 32768));
        mongoTemplate = new MongoTemplate(mongoClient, dbName);
    }

    @AfterClass
    public void deleteDb() {
        mongoClient.dropDatabase(dbName);
    }
}
