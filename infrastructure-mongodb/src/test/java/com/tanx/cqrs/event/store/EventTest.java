package com.tanx.cqrs.event.store;

import com.tanx.cqrs.event.Event;
import lombok.Data;

/**
 *
 */
@Data
public class EventTest implements Event {
    private String uuid = UuidUtils.getUuid();
    private String aggregateId = UuidUtils.getUuid();

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public String getAggregateId() {
        return aggregateId;
    }

    @Override
    public String getSagaId() {
        return null;
    }

    @Override
    public void setSagaId(String s) {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EventTest eventTest = (EventTest) o;

        return uuid != null ? uuid.equals(eventTest.uuid) : eventTest.uuid == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }
}
