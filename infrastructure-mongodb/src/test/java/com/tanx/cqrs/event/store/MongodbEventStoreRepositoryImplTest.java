package com.tanx.cqrs.event.store;

import com.tanx.cqrs.event.Event;

/**
 * 测试事件存储
 */
public class MongodbEventStoreRepositoryImplTest extends BaseTestContext {
    private MongodbEventStoreRepositoryImpl<AggregateTest, Event> repository;

//    @BeforeClass
//    public void initRepository() {
//        repository = new MongodbEventStoreRepositoryImpl<>();
//        repository.setCollectionName("EventStore");
//        repository.setMongoTemplate(mongoTemplate);
//    }
//
//    @Test
//    public void testFindByCreateDateTimeAfterAndIdByCreateDateTimeAsc() throws Exception {
//        EventTest eventTest = new EventTest();
//        EventProxy proxy = new EventProxy(eventTest, this.getClass(), this.getClass(), null);
//        repository.save(proxy);
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(new Date());
//        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
//        Date time = calendar.getTime();
//        System.out.println(time);
//        List<Event> list = repository.findByCreateDateTimeAfterAndIdByCreateDateTimeAsc(time, eventTest.getAggregateId());
//        Assert.notNull(list, "没有查找到数据");
//        Assert.isTrue(!list.isEmpty(), "集合为空");
//    }
//
//    @Test
//    public void testFindByIdByCreateDateTimeAsc() throws Exception {
//        EventTest eventTest = new EventTest();
//        EventProxy proxy = new EventProxy(eventTest, this.getClass(), this.getClass(), null);
//        repository.save(proxy);
//        List<Event> list = repository.findByIdByCreateDateTimeAsc(eventTest.getAggregateId());
//        Assert.notNull(list, "没有查找到数据");
//        Assert.isTrue(!list.isEmpty(), "集合为空");
//    }
//
//    @Test
//    public void testSave() throws Exception {
//        EventTest eventTest = new EventTest();
//        EventProxy proxy = new EventProxy(eventTest, this.getClass(), this.getClass(), null);
//        repository.save(proxy);
//        List<MongoEvent> list = mongoTemplate.findAll(MongoEvent.class, repository.getCollectionName());
//        Assert.notNull(list, "没有查找到数据");
//        Assert.isTrue(!list.isEmpty(), "集合为空");
//    }
//
//    @Test
//    public void testDelete() throws Exception {
//        Event event = new Event() {
//            private String uuid = UUID.randomUUID().toString().replaceAll("-", "");
//
//            @Override
//            public String getUuid() {
//                return uuid;
//            }
//
//            @Override
//            public String getAggregateId() {
//                return UuidUtils.getUuid();
//            }
//        };
//        EventProxy proxy = new EventProxy(event, this.getClass(), this.getClass(), null);
//        repository.save(proxy);
//        repository.delete(event);
//    }

}