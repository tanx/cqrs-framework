package com.tanx.cqrs.event.store;

import java.util.UUID;

/**
 * UUID生成器
 */
public class UuidUtils {
    public static String getUuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
