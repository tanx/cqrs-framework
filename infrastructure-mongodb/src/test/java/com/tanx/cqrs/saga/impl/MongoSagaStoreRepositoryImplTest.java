package com.tanx.cqrs.saga.impl;

import com.tanx.cqrs.event.store.BaseTestContext;
import com.tanx.cqrs.event.store.EventTest;
import com.tanx.cqrs.event.store.UuidUtils;
import com.tanx.cqrs.saga.Saga;
import com.tanx.cqrs.saga.SagaHandlerInstance;
import com.tanx.cqrs.saga.SagaInfo;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.List;

/**
 *
 */
public class MongoSagaStoreRepositoryImplTest extends BaseTestContext {
    private MongoSagaStoreRepositoryImpl repository;
    private String uuid;
    private SagaHandlerInstance instance;

    @BeforeClass
    public void initRepository() {
        repository = new MongoSagaStoreRepositoryImpl();
        repository.setMongoTemplate(mongoTemplate);
        repository.setCollectionName("sagaStore");
    }

    @Test
    public void testFindSaga() throws Exception {
        SagaInfo saga = repository.findSaga(uuid, Object.class);
        Assert.notNull(saga, "saga为空");
    }

    @Test
    public void testInsertSaga() throws Exception {
        uuid = UuidUtils.getUuid();
        Saga saga = new Saga() {
            @Override
            public String getUuid() {
                return uuid;
            }

            @Override
            public void setUuid(String sagaId) {
                uuid = sagaId;
            }
        };
        Method testInsertSaga = this.getClass().getMethod("testInsertSaga");
        instance = new SagaHandlerInstance(saga, testInsertSaga, repository, 10000);
        EventTest event = new EventTest();
        SagaInfo info = repository.insertSaga(uuid, instance, event);
        List<MongoSagaInfoImpl> all = mongoTemplate.findAll(MongoSagaInfoImpl.class, repository.getCollectionName());
        Assert.notNull(all, "未找到数据");
        Assert.isTrue(!all.isEmpty(), "集合为空");
    }

    @Test
    public void testEndSaga() throws Exception {
        testInsertSaga();
        EventTest event = new EventTest();
        repository.endSaga(uuid, instance, event);
        List<MongoSagaInfoImpl> all = mongoTemplate.findAll(MongoSagaInfoImpl.class, repository.getCollectionName());
        MongoSagaInfoImpl sagaInfo = all.get(0);
        boolean contains = sagaInfo.contains(event);
        Assert.isTrue(contains, "完成长时处理过程失败");
    }

    @Test
    public void testUpdateSagaEventList() throws Exception {
        testInsertSaga();
        EventTest event = new EventTest();
        repository.updateSagaEventList(uuid, Object.class, event);
        SagaInfo saga = repository.findSaga(uuid, Object.class);
        boolean contains = saga.contains(event);
        Assert.isTrue(contains, "不包含事件");
    }

}