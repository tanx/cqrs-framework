package com.tanx.cqrs.saga;

import com.tanx.cqrs.event.Event;

/**
 * 长时处理过程状态存储库
 */
public interface SagaStoreRepository {
    /**
     * 通过id和类型找到saga
     *
     * @param sagaId      处理过程id
     * @param sourceClass 处理过程id
     * @return 处理过程详情
     */
    SagaInfo findSaga(String sagaId, Class<?> sourceClass);

    /**
     * 新增一个长时处理过程
     *
     * @param sagaId   处理过程id
     * @param instance 处理过程
     * @param event    第一个事件
     */
    SagaInfo insertSaga(String sagaId, SagaHandlerInstance instance, Event event);

    /**
     * 结束一个长时处理过程
     *
     * @param sagaId   处理过程id
     * @param instance 处理过程
     * @param event    处理过程最后一个事件
     */
    void endSaga(String sagaId, SagaHandlerInstance instance, Event event);

    /**
     * 更新已处理事件列表
     *
     * @param sagaId      处理过程id
     * @param sourceClass 处理过程类型
     * @param event       处理过程当前事件
     */
    void updateSagaEventList(String sagaId, Class<?> sourceClass, Event event);

    /**
     * 获取一个新的uuid
     *
     * @return uuid
     */
    String newUuid();
}
