package com.tanx.cqrs.saga;


import com.tanx.cqrs.event.Event;

/**
 * 长时处理过程解析器
 */
public interface SagaHandlerResolver {

    /**
     * 注册所有的长时处理过程处理器
     * 规则1:spring bean中方法带有@SagaHandler的
     * 规则2:spring bean上注解有@Saga的
     */
    void registerAllHandler();

    /**
     * 处理事件
     *
     * @param event 事件
     */
    void handlerEvent(Event event);
}
