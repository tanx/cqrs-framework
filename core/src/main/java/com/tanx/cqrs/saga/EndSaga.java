package com.tanx.cqrs.saga;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 结束长时处理过程
 */
@SagaHandler
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EndSaga {
}
