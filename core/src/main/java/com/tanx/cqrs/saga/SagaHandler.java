package com.tanx.cqrs.saga;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 事件处理器
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
@Scope("prototype")
public @interface SagaHandler {

    /**
     * 定义长时处理过程的超时时间
     *
     * @return 时间, 单位:秒
     */
    long timeout() default -1;
}
