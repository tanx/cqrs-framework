package com.tanx.cqrs.saga;

/**
 * 抽象Saga接口
 */
public abstract class AbstractSaga implements Saga {
    protected String uuid;

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String sagaId) {
        this.uuid = sagaId;
    }
}
