package com.tanx.cqrs.saga;

/**
 * saga接口
 */
public interface Saga {
    /**
     * 获取saga的唯一标识
     *
     * @return uuid
     */
    String getUuid();

    /**
     * 设置saga唯一标识
     *
     * @param sagaId 目标id
     */
    void setUuid(String sagaId);
}
