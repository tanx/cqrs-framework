package com.tanx.cqrs.saga;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * saga存储仓库
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SagaRepository {
    /**
     * 仓库名称
     *
     * @return 仓库bean的名称
     */
    String getBeanName();
}
