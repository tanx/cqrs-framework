package com.tanx.cqrs.saga;

import com.tanx.cqrs.event.Event;

/**
 * 长时处理过程详细信息
 */
public interface SagaInfo {
    /**
     * 是否超时
     *
     * @param timeOut 超时秒,即距离现在的时间的秒数如果超过此值,则视为超时
     * @return true:超时,false:未超时
     */
    boolean timeout(long timeOut);

    /**
     * 是否已经执行过此event
     *
     * @param event 事件
     * @return true:执行过,false:未执行
     */
    boolean contains(Event event);
}
