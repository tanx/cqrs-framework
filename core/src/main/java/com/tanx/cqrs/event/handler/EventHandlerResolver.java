package com.tanx.cqrs.event.handler;

/**
 * 事件处理解析器
 */
public interface EventHandlerResolver {

    /**
     * 注册所有的事件溯源处理器
     * 规则1:spring bean中方法带有@EventHandler的
     * 规则2:spring bean上注解有@EventHandler的(暂不实现)
     */
    void registerAllHandler();

    /**
     * 处理事件
     *
     * @param event 事件
     */
    void handlerEvent(Object event);
}
