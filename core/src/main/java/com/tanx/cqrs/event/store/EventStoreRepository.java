package com.tanx.cqrs.event.store;

import com.tanx.cqrs.event.Event;
import com.tanx.cqrs.eventsourcing.snapshot.NoneSnapshotBuildStrategyImpl;
import com.tanx.cqrs.eventsourcing.snapshot.SnapshotBuildStrategy;

import java.util.Date;
import java.util.List;

/**
 * 事件基础存储库
 */
public interface EventStoreRepository<A, E extends Event> {
    /**
     * 查找快照发生后的事件
     *
     * @param createDateTime 创建事件
     * @param id             唯一标识
     * @return 事件列表
     */
    List<E> findByCreateDateTimeAfterAndIdByCreateDateTimeAsc(Date createDateTime, Object id);

    /**
     * 根据id查找事件列表
     *
     * @param id 唯一标识
     * @return 事件列表
     */
    List<E> findByIdByCreateDateTimeAsc(Object id);

    /**
     * 获取快照存储策略
     *
     * @return 快照存储策略实例
     */
    default SnapshotBuildStrategy<A> getSnapshotBuildStrategy() {
        return new NoneSnapshotBuildStrategyImpl<>();
    }

    /**
     * 存储事件
     *
     * @param event 事件
     * @return true:成功,false:失败
     */
    boolean save(Event event);

    /**
     * 删除事件
     *
     * @param event 事件
     * @return true:成功,false:失败
     */
    boolean delete(Event event);

    /**
     * 标注事件已经完成执行
     *
     * @param uuid 事件唯一标示
     */
    void finishEvent(String uuid);

    /**
     * 根据id查找事件
     *
     * @param uuid 事件唯一标识
     * @return 事件实例
     */
    E findByEventId(String uuid);

    /**
     * 查找仓库中未完成的事件
     *
     * @return 未完成的事件列表
     */
    List<Event> findByUnFinishEventByCreateDateTimeAsc();
}
