package com.tanx.cqrs.event;

import com.tanx.cqrs.saga.Saga;

/**
 * 事件总线
 * 1,发布事件到mq
 * 2,接收消息
 */
public interface EventBus {

    /**
     * 接收消息
     */
    void receiveEvent();

    /**
     * 调用消息处理
     *
     * @param event 获取到的消息代理
     */
    void invokeHandler(Event event);

    /**
     * 完成消息处理
     *
     * @param event 消息
     */
    void eventFinish(Event event);

    /**
     * 发送消息
     *
     * @param event     消息体
     * @param <T>       聚合class
     */
    <T> void sendEvent(Event event);


    /**
     * 发送消息
     *
     * @param event 消息体
     * @param saga  长时处理过程
     */
    void sendSagaEvent(Event event, Saga saga);


}
