package com.tanx.cqrs.event;

/**
 * 路由key接口
 */
public interface RoutingKeyGenerator {
    /**
     * 根据事件生成事件路由key
     *
     * @param event 事件
     * @return 路由key
     */
    String generate(Object event);
}
