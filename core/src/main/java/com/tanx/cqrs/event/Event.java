package com.tanx.cqrs.event;

/**
 * 事件接口
 */
public interface Event {

    /**
     * 获取事件的唯一标识
     *
     * @return 唯一标识
     */
    String getUuid();

    /**
     * 聚合id
     *
     * @return 聚合唯一标识
     */
    String getAggregateId();

    /**
     * 获取sagaid
     *
     * @return saga唯一标示
     */
    String getSagaId();

    void setSagaId(String s);
}
