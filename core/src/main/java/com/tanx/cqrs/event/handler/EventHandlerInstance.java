package com.tanx.cqrs.event.handler;

/**
 * 事件处理器实例
 */
public interface EventHandlerInstance {
    Class getEventClass();

    /**
     * 调用事件处理器
     *
     * @param event 事件
     * @throws Exception 异常
     */
    void invoke(Object event) throws Exception;
}
