package com.tanx.cqrs.eventsourcing.snapshot;

import java.util.Date;

/**
 * 快照接口
 */
public interface Snapshot {

    /**
     * 获取快照创建事件
     *
     * @return 快照创建事件
     */
    Date getCreateDateTime();

    /**
     * 将聚合转换为快照
     *
     * @return 快照内容
     */
    Object toSnapshotContent(Object aggregate, Object id);

    /**
     * 转换为聚合
     *
     * @return 聚合实例
     */
    Object toAggregate();


}
