package com.tanx.cqrs.eventsourcing.snapshot;

import java.util.List;

/**
 * 快照生成策略
 */
public interface SnapshotBuildStrategy<T> {
    /**
     * 是否需要创建快照
     *
     * @param aggregate 聚合
     * @param eventList 事件列表
     * @return true:需要创建
     */
    boolean needCreate(T aggregate, List eventList);

    /**
     * 创建快照
     *
     * @param aggregate 聚合
     * @param id        唯一标识
     */
    void createSnap(T aggregate, Object id);

    /**
     * 获取快照持久化仓库
     *
     * @return 持久化仓库实例
     */
    SnapshotRepository<T> getSnapshotRepository();
}
