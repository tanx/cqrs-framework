package com.tanx.cqrs.eventsourcing.snapshot;


import java.util.List;

/**
 * 默认的无快照
 */
public class NoneSnapshotBuildStrategyImpl<T> implements SnapshotBuildStrategy<T> {
    @Override
    public boolean needCreate(T aggregate, List eventList) {
        return false;
    }

    @Override
    public void createSnap(T aggregate, Object id) {
        //不需要做任何事,作为无快照版本的默认实现
    }

    @SuppressWarnings("unchecked")
    @Override
    public SnapshotRepository<T> getSnapshotRepository() {
        return new SnapshotRepository() {
            @Override
            public Snapshot findTop1SnapByCreateDateTimeDesc(Object id) {
                return null;
            }

            @Override
            public void save(Snapshot snapshot) {
                //不需要做任何事,作为无快照版本的默认实现
            }

            @Override
            public Object getAggregate(Snapshot snapshot) {
                return snapshot.toAggregate();
            }
        };
    }
}
