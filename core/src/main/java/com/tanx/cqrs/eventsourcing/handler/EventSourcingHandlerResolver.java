package com.tanx.cqrs.eventsourcing.handler;

import java.util.List;

/**
 * 事件溯源解析器
 */
public interface EventSourcingHandlerResolver {

    /**
     * 注册所有的事件溯源处理器
     */
    void registerAllHandler();

    /**
     * 事件溯源
     *
     * @param aggregate 聚合
     * @param eventList 事件列表
     */
    default void eventSourcing(Object aggregate, List eventList) {
        for (Object item : eventList) {
            eventSourcing(aggregate, item);
        }
    }


    /**
     * 单事件溯源
     *
     * @param aggregate 聚合
     * @param event     事件
     */
    void eventSourcing(Object aggregate, Object event);
}
