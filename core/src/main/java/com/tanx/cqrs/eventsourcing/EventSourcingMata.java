package com.tanx.cqrs.eventsourcing;

import lombok.Data;

/**
 * 事件溯源的源数据
 */
@Data
public class EventSourcingMata<T> {
    private Class<T> target;
    private Object id;

    public EventSourcingMata(Class<T> target, Object id) {
        this.target = target;
        this.id = id;
    }
}
