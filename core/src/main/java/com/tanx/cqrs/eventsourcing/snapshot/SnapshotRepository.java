package com.tanx.cqrs.eventsourcing.snapshot;

/**
 * 快照存储库
 */
public interface SnapshotRepository<T> {
    /**
     * 查找最后一个快照
     */
    Snapshot findTop1SnapByCreateDateTimeDesc(Object id);

    /**
     * 存快照
     *
     * @param snapshot 目标快照
     */
    void save(Snapshot snapshot);

    /**
     * 根据快照获取聚合
     *
     * @param snapshot 快照
     * @return 聚合
     */
    T getAggregate(Snapshot snapshot);
}
