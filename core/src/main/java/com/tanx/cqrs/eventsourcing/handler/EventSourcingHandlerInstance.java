package com.tanx.cqrs.eventsourcing.handler;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * 事件溯源处理器实例
 */
@Data
public class EventSourcingHandlerInstance {
    private final Object bean;
    private final Method method;
    private final Class eventClass;

    public EventSourcingHandlerInstance(Object bean, Method method) {
        this.bean = bean;
        this.method = method;
        this.eventClass = method.getParameterTypes()[0];
    }
}
