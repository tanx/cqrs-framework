package com.tanx.cqrs.eventsourcing;

import com.tanx.cqrs.event.Event;

import java.util.List;

/**
 * 事件溯源
 */
public interface EventSourcingRepository {

    <T, E extends Event> T eventSourcing(Class<T> aggregateClass, Object id);

    List<Object> eventSourcingList(List<EventSourcingMata> matas);
}
