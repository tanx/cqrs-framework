package com.tanx.cqrs.command;

import lombok.Data;

/**
 * 目标唯一标示信息
 * 键值对形式
 * 包含变量名称,参数值
 */
@Data
public class TargetAggregateIdentifierMeta {
    private TargetAggregateIdentifier identifier;
    private Object value;

    public TargetAggregateIdentifierMeta(TargetAggregateIdentifier identifier, Object value) {
        this.identifier = identifier;
        this.value = value;
    }
}
