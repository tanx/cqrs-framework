package com.tanx.cqrs.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识命令中的参数为聚合主键注解
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TargetAggregateIdentifier {
    /**
     * 在多参数中对应的变量名称
     * 主要用于领域服务中
     *
     * @return 变量名称
     */
    String value() default "";
}
