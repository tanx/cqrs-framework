package com.tanx.cqrs.command;

/**
 * 命令回调
 */
public interface CommandCallback<C, R> {
    void onSuccess(C command, R result);

    void onFailure(C command, Throwable throwable);
}
