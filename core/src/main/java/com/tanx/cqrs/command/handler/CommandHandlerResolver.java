package com.tanx.cqrs.command.handler;

import com.tanx.cqrs.command.Command;

/**
 * 命令处理解析器
 * 1.获取所有的 添加了@CommandHandler的实例
 * 2.根据命令解析并调用事件处理器
 * 负责解析注册命令道命令总线
 */
public interface CommandHandlerResolver {
    CommandHandlerInstance resolve(Command command);

    Object invoke(Command command);

    void registerAllHandler();
}
