package com.tanx.cqrs.command.handler;

import com.tanx.cqrs.command.Command;
import com.tanx.cqrs.eventsourcing.EventSourcingMata;

import java.util.List;

/**
 * 命令处理器实例
 */
public interface CommandHandlerInstance {

    /**
     * 获取要溯源的信息
     *
     * @return 要溯源的信息列表
     */
    List<EventSourcingMata> getEventSourcingMetas(Command command);

    /**
     * 调用此命令处理器
     */
    Object invoke(Command command, Object... parameters);

}
