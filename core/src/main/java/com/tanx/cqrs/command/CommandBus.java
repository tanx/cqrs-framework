package com.tanx.cqrs.command;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 命令总线
 * 负责命令分发
 */
public interface CommandBus {
    void send(Command command, CommandCallback<? super Command, Object> commandCallback);

    Object sendAndWait(Command command) throws ExecutionException, InterruptedException;

    Object sendAndWait(Command command, long time, TimeUnit timeUnit);

    CompletableFuture<Object> send(Command command);
}
